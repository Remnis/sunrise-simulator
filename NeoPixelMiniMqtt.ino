
// ESP 8266
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// ESPName
char* EspName = "NeopixelMini-1";

// WiFi & MQTT Server
const char* ssid = "****";
const char* password = "***";
const char* mqtt_server = "192.168.1.99";

// MQTT Subscribtions
const char* SubscribeModeIn = "neopixel/mini-1/mode/in"; // ESP recieves here mode info
const char* SubscribeModeOut = "neopixel/mini-1/mode/out";

const char* SubscribeTimerIn = "neopixel/mini-1/timer/in"; // ESP recieves here timer info
const char* SubscribeTimerOut = "neopixel/mini-1/timer/out";

const char* SubscribePercentageOut = "neopixel/mini-1/percent/out"; // output of time elapsed

WiFiClient espClient;
PubSubClient pubClient(espClient);

// Neopixels
#include <Adafruit_NeoPixel.h>
#define NeoPIN D6
#define NUM_LEDS 5
int brightness = 180;
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, NeoPIN, NEO_RGB + NEO_KHZ800);
String lastColor = "#000000";

// mode 
int wallMode = 0;
long lastPub = millis();
long maxPubTime = 1200;
//sunset
long sunsetTimer = millis();
bool sunsetStarted = false;
long sunsetDuration = 60000;
int lastPercentage = 0;
// 

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] !");


  // changing mode
  if(String(topic).equals(String(SubscribeModeIn))){
    
    String m("");
    for (int i = 0; i < length; i++) {
        m = m + String((char)payload[i]);
    }
    // reset
    if(wallMode != m.toInt() && m.toInt() == 1){
      sunsetStarted = true;
      sunsetTimer = millis();
      lastPercentage = 0;
    }
    wallMode = m.toInt();
    char b[2];
    String str;
    str=String(wallMode);
    str.toCharArray(b,2);
    pubClient.publish(SubscribeModeOut, b );
  }
  // setting sunrise duration
  else if(String(topic).equals(String(SubscribeTimerIn))){
    
    String m("");
    for (int i = 0; i < length; i++) {
        m = m + String((char)payload[i]);
    }
    long maxtimer = m.toInt(); // * 60; // minutes
    sunsetDuration = maxtimer * 1000; // to millisecons
    Serial.print(sunsetDuration, DEC);
    Serial.println(" sec");
    char b[5];
    String str;
    str=String(sunsetDuration);
    Serial.println(str + " min");
    str.toCharArray(b,5);
    pubClient.publish(SubscribeTimerOut, b );
  }
  else{
    Serial.println("No fitting topic found");
  }

  
  Serial.println();

}


void setup() {
  Serial.begin(115200);
  // Neopixel
  strip.setBrightness(brightness);
  strip.begin();
  strip.show();
  delay(50);
  // WiFi
  setup_wifi();

   // MQTT
    Serial.print("Connecting to MQTT...");
    // connecting to the mqtt server
    pubClient.setServer(mqtt_server, 1883);
    pubClient.setCallback(callback);
    Serial.println("done!");
}

void loop() {
  // PubSub connection
  if (!pubClient.connected()) {
    delay(100);
    reconnect();
  }
  pubClient.loop();
  
  checkMode();
  publishTopics();
}




// Network connection
void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

// MQTT server connect
void reconnect() {
  // Loop until we're reconnected
  while (!pubClient.connected()) {
    // Attempt to connect
    if (pubClient.connect(EspName)) {
      Serial.println("connected");
      // subscribe / pub mode
      pubClient.subscribe(SubscribeModeIn);
      pubClient.publish(SubscribeModeOut, "0");
      pubClient.loop();
      // subscribe / publish to timer
      pubClient.subscribe(SubscribeTimerIn);
      pubClient.publish(SubscribeTimerOut, "60");
      pubClient.loop();
      // publish percentage past
      pubClient.subscribe(SubscribePercentageOut);
      pubClient.loop();
      
      
    } else {
      Serial.print("failed, rc=");
      Serial.print(pubClient.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void publishTopics(){
    
  if((lastPub + maxPubTime) < millis()){
    // publish mode
    char b[2];
    String str;
    str=String(wallMode);
    str.toCharArray(b,2);
    pubClient.publish(SubscribeModeOut, b );
    Serial.println(str);
    // last percentage
    char p[3];
    str=String(lastPercentage);
    str.toCharArray(p,3);
    pubClient.publish(SubscribePercentageOut, p );

    // duration
    char sd[5];
    int ssd = sunsetDuration / 1000;
    str=String(ssd);
    str.toCharArray(sd,5);
    pubClient.publish(SubscribeTimerOut, sd );
  
    lastPub = millis();
  } 
}



void checkMode(){
  switch(wallMode){
    / off
    case 0:
    if(lastPercentage == -1){
      setNeoColor(0,0,0);
      lastPercentage = 0;
     }
      break;
    case 1:
      // sunrise mode
      if(lastPercentage < 100){
        sunrise();
      }
      else {
        wallMode = 0;
        lastPercentage = -1;
      }
      break;
  }
}


void sunrise(){
  int timePassed = millis() - sunsetTimer;
  int percent = (timePassed * 100) / sunsetDuration;

  if(percent != lastPercentage){
    int r = map(percent, 0, 33, 0, 255);
    int g = map(percent, 33, 66, 0, 255);
    int b = map(percent, 66, 100, 0, 255);
    if(r > 255){
      r = 255;
    }
    if(g < 0){
      g = 0;
    }
    else if (g > 255){
      g = 255;
    }
    if(b < 0 ){
      b = 0;
    }
    else if( b > 255){
      b = 255;
    }
    setNeoColor(r,g,b);
    lastPercentage = percent;
  }

}


// setting all Neopixels to one color
void setNeoColor(int r, int g, int b){
    Serial.println("Setting Neopixel..." );
    Serial.print(r, DEC);
    Serial.print(",");
    Serial.print(g, DEC);
    Serial.print(",");
    Serial.println(b, DEC);
     for(int i=0; i < NUM_LEDS; i++) {
      strip.setPixelColor(i, strip.Color( g, r, b ) );
    }
    strip.show();
    Serial.println("on.");
}

 
